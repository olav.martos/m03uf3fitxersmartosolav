﻿/*
 * AUTHOR: Olav Martos
 * DATE: 08/02/2023
 * DESCRIPTION: Conjunt d'exercisis per poder practicar la gestio de fitxers amb C#
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Fitxers
{
    class Fitxers
    {
        static void Main()
        {
            var menu = new Fitxers();
            menu.Menu();
        }

        public void Menu()
        {
            var option = "";
            string[] exercise = { "Sortir del menu", "EvenAndOdd", "Inventary", "WordCount", "ConfigFile Language", "FindPaths" };
            do
            {
                Console.Clear();
                showExercise(exercise);
                Console.Write("Escull una opcio: ");

                option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        Console.Clear();
                        programExecuted(exercise, option);
                        EvenAndOdd();
                        break;
                    case "2":
                        Console.Clear();
                        programExecuted(exercise, option);
                        Inventary();
                        break;
                    case "3":
                        Console.Clear();
                        programExecuted(exercise, option);
                        WordCount();
                        break;
                    case "4":
                        Console.Clear();
                        programExecuted(exercise, option);
                        ConfigLang();
                        break;
                    case "5":
                        Console.Clear();
                        programExecuted(exercise, option);
                        FindPaths();
                        break;
                    case "0":
                    case "END":
                    case "end":
                    case "cierrate":
                    case "ADIOS":
                    case "adios":
                    case "EXIT":
                    case "Exit":
                    case "exit":
                    case "FIN":
                    case "fin":
                    case "Fin":
                        Console.Clear();
                        endExercise();
                        Environment.Exit(0);
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opció incorrecta!");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
                Console.ReadLine();
            } while (option != "0");

        }

        public void showExercise(string[] ex)
        {
            for (int i = 0; i < ex.Length; i++) Console.WriteLine($"{i}. - {ex[i]}");
        }

        public void endExercise()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa una tecla per terminar");
            Console.ForegroundColor = ConsoleColor.White;
            return;
        }

        public void programExecuted(string[] exercise, string option)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            int index = Convert.ToInt32(option);
            Console.Write("Programa executat: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{exercise[index]}");
            Console.ResetColor();
        }



        /* 
         * Nos dan un fichero con números enteros
         * • Leer el fichero origen Numbers.txt
         * • Generar un fichero que contenga sólo los numeros pares y otro fichero para
         * numeros impares(los números han de estar ordenados de mayor a menor)
         * Requisitos para modular:
         * Crear un método llamado ParellSenar que devuelve "parell.txt" o "sencer.txt" y recibe
         * la ruta del directorio donde se encuentra el fichero "Numbers.txt"
         * Y un método IsParell que recibe el dato leído y devuelve true si es un número par y
         * false si es impar.
        */
        public void EvenAndOdd()
        {
            // Insertamos la ruta
            Console.Write("Inserte la ruta: ");
            string path = Console.ReadLine();

            // Se le añade el fichero a analizar.
            string fullpath = path + @"\Numbers.txt";

            // Si el fichero existe en esta ruta, se borraran los archivos parell.txt i sencer.txt en caso de existir
            if (File.Exists(fullpath))
            {
                string fullPath1 = Path.GetFullPath("../parell.txt", fullpath);
                string fullPath2 = Path.GetFullPath("../sencer.txt", fullpath);
                File.Delete(fullPath1);
                File.Delete(fullPath2);

                // Ejecutamos una funcion que guardara los numeros en su archivo y nos devolvera el nombre del archivo
                string parellSenar = ParellSenar(fullpath, fullPath1, fullPath2);

                string[] listPS = parellSenar.Split(",");

                // Decimos donde se encuentras los dos archivos
                Console.WriteLine($"Pares creados correctamente en el fichero {path + @"\" + listPS[0]}.");
                Console.WriteLine($"Impares creados correctamente en el fichero {path + @"\" + listPS[1]}.");
            }
            // En caso de no existir, dira que no existe
            else
            {
                Console.WriteLine($"El archivo {path} no existe, no es real.");
            }
            endExercise();
        }

        public string ParellSenar(string path, string pathEven, string pathOdd)
        {
            // Creamos dos listas, una para los pares y otra para los impares
            List<int> evens = new List<int>();
            List<int> odds = new List<int>();

            // Abrimos el archivo de los numeros
            StreamReader sr = File.OpenText(path);
            string st = sr.ReadToEnd();

            // Pasamos esa string a una array y le quitamos cuando se pulsa la Tecla Enter (\r) y la creacion de nuevas lineas
            string[] s = st.Split("\r\n");
            // Eliminamos los null que pueda haber
            s = s.Where(num => !string.IsNullOrEmpty(num)).ToArray();

            //Cerramos
            sr.Close();

            // Recorremos la array y guardamos cada numero en una lista dependiendo de si son pares o no
            for (int i = 0; i < s.Length; i++)
            {
                if (IsParell(s[i])) evens.Add(Convert.ToInt32(s[i]));
                else odds.Add(Convert.ToInt32(s[i]));
            }

            // Invertimos las listas
            evens.Reverse();
            odds.Reverse();

            // Llamamos a una funcion que escribira cada linea de la lista pasada a la ruta del archivo
            WriteInFile(evens, pathEven);
            WriteInFile(odds, pathOdd);

            // Devolvemos el nombre de los archivos
            return "parell.txt,sencer.txt";
        }

        /// <summary>
        /// Funcion booleana que comprobara en una sola linea si el numero pasado es par o impar
        /// </summary>
        /// <param name="s">Numero pasado que estaba en formato string</param>
        /// <returns>Devuelve True si es par. False si es impar</returns>
        public bool IsParell(string s) { return (Convert.ToInt32(s) % 2 == 0); }

        /// <summary>
        /// Escribe en cada linea del archivo, uno de los numero de la lista
        /// </summary>
        /// <param name="numbers">Lista pasada</param>
        /// <param name="path">Ruta del archivo donde se van a guardar</param>
        public void WriteInFile(List<int> numbers, string path)
        {
            foreach (var i in numbers)
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(i);
                }
            }
        }



        /*
         * Crea un programa para gestionar un inventario, hazlo persistente guardando los datos 
         * en un fichero txt. Separa los campos utilizando una coma. 
         * Cuando se añade un nuevo elemento al fichero, los registrados anteriormente o en otra 
         * sesión no deben desaparecer. Puedes ayudarte del método File.AppendText(filePath) 
         * Permite que el usuario pueda listar los elementos del inventario.
         */
        public void Inventary()
        {
            // Preguntamos la ruta del archivo Inventary.txt
            Console.Write("Introduce la ruta donde se encuentra Inventary.txt: ");
            string path = Console.ReadLine();

            string fullPath = path + @"\Inventary.txt";
            // Si no existe el archivo en esa ruta, lo creamos
            if (!File.Exists(fullPath))
            {
                File.CreateText(fullPath);
                Console.WriteLine("Se ha creado el archivo. Vuelve a ejecutarnos");
                // Salimos del programa debido a que no deja insertar lineas por que cree que esta ocupado en otro proceso
                endExercise();
                Environment.Exit(0);
            }
            // Si existe se ejecutara la funcion SubInventary()
            else
            {
                SubInventary(fullPath);
            }
            endExercise();
        }

        // Muestra un menu con acciones que hacer con el archivo Inventary.txt:
        // Añadir una nueva linea VarUser()
        // O listar todas las lineas del archivo ShowInventary()
        void SubInventary(string fullpath)
        {
            string[] subExercise = { "Salir", "Añadir linea", "Listar" };
            Console.WriteLine("Que desea hacer");
            showExercise(subExercise);
            var option = Console.ReadLine();

            do
            {
                switch (option)
                {
                    case "0":
                        break;
                    case "1":
                        VarUser(fullpath);
                        break;
                    case "2":
                        ShowInventary(fullpath);
                        break;
                }

                Console.WriteLine("\nSi desea continuar, pulse cualquier tecla");
                Console.ReadLine();
                Console.Clear();
                Console.WriteLine("Que desea hacer? ");
                showExercise(subExercise);
                option = Console.ReadLine();

            } while (option != "0");
        }

        /// <summary>
        /// Funcion que llama a otras dos funciones, una que almacena lo que escribe el usuario
        /// <para>Y otra que escribe lo del usuario en el archivo Inventary.txt</para>
        /// </summary>
        /// <param name="fullPath">Ruta del txt</param>
        void VarUser(string fullPath)
        {
            string name = AskUser("el nombre");
            string model = AskUser("el modelo");
            string mac = AskUser("el MAC");
            int fabriYear = Convert.ToInt32(AskUser("el año de fabricacion"));

            WriteFile(name, model, mac, fabriYear, fullPath);
        }

        /// <summary>
        /// Le dice al usuario que introduzca los datos que corresponden
        /// </summary>
        /// <param name="varUser">Parametro que indica que dato tienen que poner</param>
        /// <returns>El dato escrito del usuario</returns>
        public string AskUser(string varUser)
        {
            Console.Write($"Introduce {varUser}: ");
            return Console.ReadLine();
        }

        // Escribe en el archivo Inventary.txt, los datos del usuario.
        void WriteFile(string name, string model, string mac, int fabriYear, string fullPath)
        {
            using (StreamWriter sw = File.AppendText(fullPath))
            {
                sw.Write($"{name},{model},{mac},{fabriYear}\n");
            }
        }

        // Lista cada linea dentro del archivo Inventary.txt.
        void ShowInventary(string fullpath)
        {
            using (StreamReader sr = File.OpenText(fullpath))
            {
                foreach (string item in File.ReadLines(fullpath))
                {
                    Console.WriteLine($"> {item}");
                }
            }
        }



        /*
         * Contar apariciones de una palabra en un fichero de texto wordcount.txt 
         * Se pide que:
         * El usuario pueda introducir por consola una o varias palabras separadas por espacio, y le
         * imprimiremos por pantalla cuántas ocurrencias para cada palabra hay en el texto. La
         * simulació acaba amb l'acció END.
         */
        public void WordCount()
        {
            Console.Write("Inserte la ruta donde se encuentra el archivo wordcount.txt: ");
            string path = Console.ReadLine();
            string fullPath = path + @"\wordcount.txt";
            if (!File.Exists(fullPath))
            {
                using (StreamWriter sw = File.AppendText(fullPath))
                {
                    sw.WriteLine("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc");
                }
                Console.WriteLine($"No se ha encontrado ningun archivo wordcount.txt en {path}. Por lo que se ha creado con un texto Lorem Ipsum por defecto");
                Console.WriteLine("Vuelve a ejecutar el programa por favor");
            }
            else
            {
                // Guardamos el contenido del fichero en una string y la pasamos a una funcion donde introduciremos las palabras
                string text = File.ReadAllText(fullPath);
                InputWord(text);
            }
            endExercise();
        }

        /// <summary>
        /// Funcion donde preguntamos al usuario las palabras que quiere buscar y que si quiere dejar de buscar escriba END
        /// </summary>
        /// <param name="text">String que contiene el contenido del fichero despues de leer todo el texto</param>
        void InputWord(string text)
        {
            // Preguntaremos las palabras
            Console.WriteLine("Introduce una o varias palabras separadas por espacio");
            Console.WriteLine("Para acabar, escriba END");
            Console.Write("> ");


            string input = Console.ReadLine();

            text = text.ToLower();

            Console.WriteLine();

            // Si el input del usuario es diferente a "end" se ejecutara
            while (input.ToLower() != "end")
            {
                // Separamos las palabras del usuario
                string[] searchWords = input.Split();
                // Y hacemos un bucle donde llamaremos a una funcion que contara las palabras
                foreach (string word in searchWords)
                {
                    Counter(text, word);
                }

                // Dejaremos que vuelva a escribir
                Console.Write("\n> ");
                input = Console.ReadLine();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Funcion que muestra el numero de coincidencias de las palabras buscadas
        /// <para>Si no encuentra ninguna, dira que no se encuentra en el texto</para>
        /// </summary>
        /// <param name="text">String que contiene el contenido del fichero</param>
        /// <param name="word">Una de las palabras que estamos buscando</param>
        void Counter(string text, string word)
        {
            // Si el numero de coincidencias es mayor a 0 mostrara la palabra y el numero de veces que se ha encontrado
            if ((Regex.Matches(text, word).Count) > 0) { Console.WriteLine($"{word}: {Regex.Matches(text, word).Count}"); }
            // Si no dira que no existe
            else { Console.WriteLine($"La palabra '{word}' no se encuentra en el texto."); }
        }



        /*
         * Queremos configurar nuestro programa utilizando un fichero de configuración: Fichero adjunto en el classroom
         * Crea un programa que utilice los valores del fichero config.txt y permita seleccionar el 
         * idioma en el que nos comunicaremos con el usuario
         */
        void ConfigLang()
        {
            Console.WriteLine("Si quiere acabar escriba END");
            Console.Write("\nEscribe la ruta donde esta config.txt y donde esta el directorio lang con sus archivos: ");
            string semipath = Console.ReadLine();

            // Ruta del config.txt y del directorio \lang
            string path = semipath + @"\config.txt";
            string otherPath = semipath + @"\lang";

            // Preguntamos si existen el archivo y el directorio
            if (Existence(path, otherPath))
            {
                string text = File.ReadAllText(path);
                string[] textS = text.Split(new char[] { ' ', '\r', '\n', '.', ',', ';', ':', '!', '?', '(', ')', '{', '}', '[', ']', '<', '>', '-', '_', '+', '=', '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);

                // Preguntamos el idioma que quiere el usuario
                Console.Write(">Idioma ");
                string idioma = Console.ReadLine();

                // Creamos un bucle para ir preguntando al usuario hasta que escriba END
                while (idioma.ToUpper() != "END")
                {
                    // Buscamos el archivo con el idioma que el usuario ha indicado que quiere
                    string lng = "lng_" + idioma + ".txt";
                    string lngPath = @"\" + lng;

                    // Comprovamos si existe dentro del directorio lang
                    if (File.Exists(otherPath + lngPath))
                    {
                        Saludar(textS, otherPath, lngPath);
                    }
                    // Si no existe, se sustituira por el idioma por defecto del documento
                    else
                    {
                        lng = "lng_" + textS[3] + ".txt";
                        lngPath = @"\" + lng;
                        Saludar(textS, otherPath, lngPath);
                    }
                    // Volvemos a preguntar
                    Console.Write("\n>Idioma ");
                    idioma = Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Vuelva a ejecutarnos");
            }

            endExercise();
        }

        /// <summary>
        /// Funcion booleana que revisa la existencia de los archivos necesarios
        /// </summary>
        /// <param name="path">Ruta del fichero config.txt</param>
        /// <param name="otherPath">Ruta del directorio \lang</param>
        /// <returns></returns>
        bool Existence(string path, string otherPath)
        {
            // Si el archivo no existe, crea un archivo config con nombre USER y idioma por defecto cat
            if (!File.Exists(path))
            {
                Console.WriteLine("Hemos creado el archivo config.txt al no existir");
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine("Nombre:USER");
                    sw.WriteLine("Idioma:cat");
                }
                // Devolvemos false para volver a ejecutar todo el programa de cero
                return false;
            }
            // Si el directorio no existe, lo creamos y en su interior le creamos los archivos lng_cat.txt y lng_es.txt
            if (!Directory.Exists(otherPath))
            {
                Console.WriteLine("Hemos creado el directorio lang junto a dos archivos de lenguaje lng_cat y lng_es");
                Directory.CreateDirectory(otherPath);
                using (StreamWriter sw = File.AppendText(otherPath + @"\lng_cat.txt"))
                {
                    sw.Write("Bonna tarda ");
                }
                using (StreamWriter sw = File.AppendText(otherPath + @"\lng_es.txt"))
                {
                    sw.Write("Buenas tardes ");
                }
                // Devolvemos false para volver a ejecutar todo el programa de cero
                return false;
            }
            // Si los dos existen, devolvemos true, para que el resto del programa se ejecute
            return true;
        }

        /// <summary>
        ///  Funcion que usamos para mostrar el saludo en el idioma correspondiente
        /// </summary>
        /// <param name="textS">Es el archivo por defecto, se usa para obtener el nombre de la persona</param>
        /// <param name="otherPath">Ruta del directorio \lang</param>
        /// <param name="lngPath">Es el idioma en el que va a hablar, puede ser el por defecto o el que ha introducido el usuario</param>
        void Saludar(string[] textS, string otherPath, string lngPath)
        {
            using (StreamReader sr = File.OpenText(otherPath + lngPath))
            {
                string file = sr.ReadToEnd();
                foreach (char line in file)
                {
                    Console.Write(line);
                }
            }
            Console.Write(textS[1]);
        }



        /*
         * Feu un programa que pregunti per pantalla el nom d’un fitxer i el text de la ruta a una
         * carpeta. Aquestes dades les introduirà l’usuari pel teclat. Llavors el programa ha de
         * cercar i mostrar per pantalla la ruta absoluta de tots els fitxers amb aquest nom a partir
         * de la carpeta assenyalada (tant directament dintre seu com dins d’altres carpetes successives).
         */
        public void FindPaths()
        {
            // Preguntamos al usuario el nombre del archivo a buscar y la ruta donde buscar
            string nameFile = AskUser("el nombre del fichero a buscar");
            string path = AskUser("la ruta de una carpeta donde mirar");

            //FindPathWithQuery(nameFile, path);
            FindPathWithoutQuery(nameFile, path);
            endExercise();
        }

        // Funcion que usa una query para encontrar el archivo
        public void FindPathWithQuery(string nameFile, string path)
        {
            // Guardamos el contenido de la ruta en una variable
            DirectoryInfo dir = new DirectoryInfo(path);

            // Guardamos todos los ficheros que contiene
            IEnumerable<FileInfo> list = dir.GetFiles("*.*", SearchOption.AllDirectories);
            // Creamos una consulta donde se guardaran los datos de los archivos encontrados con los que busca el usuario
            IEnumerable<FileInfo> query =
                from file in list
                where file.Name == nameFile
                orderby file.Name
                select file;

            ShowQuery(query, nameFile, path);
        }

        // Veremos si la consulta realizada esta vacia o no
        public void ShowQuery(IEnumerable<FileInfo> query, string nameFile, string path)
        {
            // Si no esta vacia nos devolvera la ruta de cada archivo encontrado
            if (query.Any())
            {
                foreach (FileInfo file in query)
                {
                    Console.WriteLine($"Se ha encontrado el archivo en: {file.FullName}");
                }
            }
            // Si no dira que no existe ninguna archivo en esa ruta
            else
            {
                Console.WriteLine($"No existe ningun archivo {nameFile} en {path}");
            }
        }

        // Funcion que busca el archivo en concreto de forma recursiva
        public void FindPathWithoutQuery(string nameFile, string path)
        {
            if (Directory.Exists(path))
            {
                CercaFitxers(nameFile, path);
            }
            else
            {
                Console.WriteLine("La carpeta indicada no existeix.");
            }
        }

        // Funcion recursiva para encontrar el archivo deseado
        public void CercaFitxers(string nameFile, string path)
        {
            string[] fitxers = Directory.GetFiles(path, nameFile);
            foreach (string fitxer in fitxers)
            {
                Console.WriteLine($"Se ha encontrado el archivo en: {fitxer}");
            }

            string[] subcarpetes = Directory.GetDirectories(path);
            foreach (string subcarpeta in subcarpetes)
            {
                CercaFitxers(nameFile, subcarpeta);
            }
        }
    }
}
